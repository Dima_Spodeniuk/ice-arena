import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimpleNotificationsModule } from 'angular2-notifications';

import { AppRoutes } from './app.routing';

import { APP_PREBOOT } from '../preboot';

import { TOKEN_API_INJECTOR } from '@helpers/token-api-injector.service';

import { SharedPipesModule } from '@pipes/shared-pipes.module';

import { AppComponent } from './app.component';
import { AdminComponent } from './admin/admin.component';
import { DefaultComponent } from './default/default.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    DefaultComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    SimpleNotificationsModule.forRoot(),
    SharedPipesModule,
    AppRoutes
  ],
  providers: [
    //APP_PREBOOT,
    //TOKEN_API_INJECTOR
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
