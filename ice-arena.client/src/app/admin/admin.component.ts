import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

import { Store } from '@store';
import { AuthTokenMethods } from '@reducers/auth-token.reducer';

import { NotificationTypeEnum } from '@enums/notification.enum';

import { Login } from '@classes/login.class';
import { Config } from '@classes/config.class';
import { AppNotification } from '@classes/app-notification.class';

import { LoginService } from '@services/login.service';
import { ConfigService } from '@services/configs.service';

import { NotificationSubject } from '@subjects/notification.subject';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})

export class AdminComponent implements OnInit {
  loginForm:FormGroup;
  configForm:FormGroup;
  authToken:string;
  tokenHelper:JwtHelperService;
  loginErrors:string[];
  configs:Config;

  constructor(
    private router:Router,
    private formBuilder:FormBuilder,
    private loginService:LoginService,
    private configService:ConfigService,
    private notificationSubject:NotificationSubject
  ) {
    this.tokenHelper = new JwtHelperService();
  }

  ngOnInit() {
    //this.getAuthToken();
    this.getConfigs();
    this.buildLoginForm();
    this.buildConfigForm();
  }

  getAuthToken() {
    this.authToken = Store.getState().AuthTokenReducer;
    Store.subscribe(() => this.authToken = Store.getState().AuthTokenReducer);
    if(this.authToken) this.getConfigs();
  }

  getConfigs() {
    this.configService.get().subscribe(data => {
      this.configs = data;
      if(data) this.patchConfigForm();
    });
  }

  buildLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  buildConfigForm() {
    this.configForm = this.formBuilder.group({
      dataLink: ['', Validators.required]
    });
  }

  patchConfigForm() {
    this.configForm.patchValue({
      dataLink: this.configs.dataLink
    });
  }

  onUpdateConfigs() {
    if(this.configForm.valid) {
      let data = new Config({
        id: this.configs && this.configs.id ? this.configs.id : null,
        dataLink: this.configForm.value.dataLink
      });

      this.configService.post(data).subscribe(res => {
        let notification = new AppNotification({
          type: res ? NotificationTypeEnum.success : NotificationTypeEnum.error,
          title: 'Configs',
          text: res ? 'Successfully updated.' : 'Some error'
        });

        this.notificationSubject.create(notification);
      });
    }
  }

  onLogout() {
    Store.dispatch({ type: AuthTokenMethods.remove });
    this.router.navigateByUrl('');
  }

  onCancel() {
    this.router.navigateByUrl('');
  }

  onLogin() {
    this.loginErrors = [];
    if(this.loginForm.valid) {
      let data = new Login({
        email: this.loginForm.value.email,
        password: this.loginForm.value.password
      });

      this.loginService.post(data).subscribe(res => {
        if(res.isSuccess) {
          Store.dispatch({ type: AuthTokenMethods.save, payload: res.result });
          this.getConfigs();
        } else {
          this.loginErrors = res.errors;
        }
        this.loginForm.reset();
      });
    }
  }
}
