import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { CalendarService } from '@services/calendar.service';
import { DateConvertService } from '@helpers/date-convert.service';

interface IItem {
  name:string;
  fromStr : string;
  toStr : string;
  from:Date;
  to:Date;
  state:string;
  lockerRoom:string;
}

@Component({
  selector: 'app-default',
  templateUrl: './default.component.html',
  styleUrls: ['./default.component.scss'],
  animations: [
    trigger('slide', [
      state('show', style({})),
      state('hide', style({ marginTop: '-166px' })),
      transition('show => hide', animate('500ms ease-in-out')),
      transition(':enter', [
        style({transform: 'translateX(100%)', opacity: 0}),
        animate('500ms', style({transform: 'translateX(0)', opacity: 1}))
      ]),
    ])
  ],
  providers : [DateConvertService]
})

export class DefaultComponent implements OnInit {
  today:Date;
  data:string;
  north:IItem[];
  south:IItem[];
  startSouthAnimation:boolean;
  startNorthAnimation:boolean;
  runStringDelay:number;
  isRunStringTimerInit:boolean;

  constructor(
    private dateConvertService :  DateConvertService,
    private calendarService:CalendarService
  ) {
    this.runStringDelay = 4000;
    this.startSouthAnimation = false;
    this.startNorthAnimation = false;
    this.isRunStringTimerInit = false;
  }
  
  ngOnInit() {
    this.dataTimer();
    this.timeTimer();
  }

  getCalendar() {
    this.calendarService.get().subscribe(data => {
      if (data !== this.data) {
        this.data = data;
        this.processData();
      }
    });
  }

  getTime() {
    this.today = new Date();
  }

  processData() {
    if(this.data) {
      let data = this.data;
      let blocks:string[] = [];

      for(let i = 0; i < data.length; i++) {
        let start = data.indexOf("BEGIN:VEVENT");
        let end = data.indexOf("END:VEVENT");
        let forCut = data;
        blocks.push(forCut.slice(start, end));
        data = data.slice((end + 1), data.length);
      }
      this.parseBlocks(blocks);
    }
  }

  parseBlocks(data:string[]) {
    this.south = [];
    this.north = [];

    for(let item of data) {
      let from = item.indexOf("DTSTART:");
      let to = item.indexOf("DTEND:");
      let descIndex = item.indexOf("DESCRIPTION:");
      let description = item.substring(descIndex);
      description = description.replace(/(\r\n\t|\n|\r\t)/gm,"");
      let titleFrom = description.indexOf("Title:");
      let title = description.substr(titleFrom + 7);
      title = title.substr(0, title.indexOf("\\n"));
      let fromStr =  item.slice((from + 8), (from + 24));
      let toStr = item.slice((to + 6), (to + 22));
      let fromDate = this.parseDate(fromStr);
      let toDate = this.parseDate(toStr);
      console.log(fromStr, title);
      let obj:IItem = {
        name: title,
        fromStr : fromStr,
        toStr : toStr,
        from: fromDate,
        to: toDate, 
        lockerRoom: this.getLockerRoom(item),
        state : "show"
      }

      if(item.includes("South Rink")) {
        this.south.push(obj);
      }
      if(item.includes("North Rink")) {
        this.north.push(obj);
      }
    }
    this.filterData();
  }

  parseDate(value : string) : Date {
    let year = value.slice(0, 4);
    let month = value.slice(4, 6);
    let day = value.slice(6, 8);
    let hours = value.slice(9, 11);
    let minutes = value.slice(11, 13);
    let seconds = value.slice(13, 15);
    let date = new Date(parseInt(year), (parseInt(month) - 1), parseInt(day), parseInt(hours), parseInt(minutes), parseInt(seconds));
    date = this.dateConvertService.fromLocalToGMT(date);
    return date;
  }


  getLockerRoom(data:string):string {
    if(data.includes('LR 1')) {
      return "LR 1";
    } else if(data.includes('LR 2')) {
      return "LR 2";
    } else if(data.includes('LR 3')) {
      return "LR 3";
    } else if(data.includes('LR 4')) {
      return "LR 4";
    } else if(data.includes('LR 5')) {
      return "LR 5";
    } else if(data.includes('LR 6')) {
      return "LR 6";
    } else if(data.includes('LR 7')) {
      return "LR 7";
    } else if(data.includes('LR 8')) {
      return "LR 8";
    } else if(data.includes('LR 9')) {
      return "LR 9";
    } else {
      return null;
    }
  }

  fromLocaltoGMT(date: Date): Date {
    let localDate = new Date(date.getTime() + (60000 * date.getTimezoneOffset()));
    return localDate;
  }

  filterData() {
    let dateNow = new Date();
    let dateTomorrow = new Date();
    dateTomorrow.setDate(dateTomorrow.getDate() + 1);
    dateTomorrow.setHours(0, 0, 0, 0);
    dateTomorrow = this.fromLocaltoGMT(dateTomorrow);

    this.north = this.north.filter(i => i.from < dateTomorrow && i.to > dateNow);
    this.south = this.south.filter(i => i.from < dateTomorrow && i.to > dateNow);

    this.runStringTimer();
  }

  dataTimer() {
    this.getCalendar();
    setInterval(() => this.getCalendar(), 60000);
  }

  timeTimer() {
    this.getTime();
    setInterval(() => this.getTime(), 1000);
  }

  runStringTimer() {
    if(!this.isRunStringTimerInit) {

      setInterval(() => {
        this.startSouthAnimation = true;
        this.startNorthAnimation = true;
        if(this.south && this.south.length) this.south[0].state = "hide";
        if(this.north && this.north.length) this.north[0].state = "hide";
      }, this.runStringDelay);

      this.isRunStringTimerInit = true;
    }
  }

  animationSouthEnd() {
    if(this.startSouthAnimation) {
      let item = { ...this.south[0] };
      this.south.splice(0, 1);
      this.startSouthAnimation = false;
      item.state = "show";
      this.south.push(item);
    }
  }

  animationNorthEnd() {
    if(this.startNorthAnimation) {
      let item = { ...this.north[0] };
      this.north.splice(0, 1);
      this.startNorthAnimation = false;
      item.state = "show";
      this.north.push(item);
    }
  }
}