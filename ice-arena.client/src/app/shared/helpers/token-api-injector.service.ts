import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Store } from '@store';
 
export class TokenApiInjector implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let token:string = Store.getState().AuthTokenReducer;

        Store.subscribe(() => token = Store.getState().AuthTokenReducer as string);

        if(token) {
            request = request.clone({
                setHeaders: { 
                    Authorization: `Bearer ${token}`
                }
            });
            request.clone({ headers: request.headers.set('Authorization', `Bearer ${token}`) });
        }
 
        return next.handle(request);
    }
}

export const TOKEN_API_INJECTOR = {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenApiInjector,
    multi: true
}