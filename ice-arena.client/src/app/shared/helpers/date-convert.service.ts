import { Injectable } from "@angular/core";

@Injectable({ 	providedIn: "root" })
export class DateConvertService {

  constructor() { }

  resetHours(date:Date): Date {
    let result:Date = new Date(date);
    result.setHours(0, 0, 0, 0);
    return result;
  }

  fromLocalToGMT(date: Date): Date {
    let gmtDate = new Date(date.getTime() - (60000 * date.getTimezoneOffset()));
    return gmtDate;
  }

  fromGMTtoLocal(date: Date): Date {
    let localDate = new Date(date.getTime() + (60000 * date.getTimezoneOffset()));
    return localDate;
  }

  getTodayGMT() : Date {
    let today = new Date();
    today = this.resetHours(today);
    today = this.fromLocalToGMT(today);
    return today;
  }

  getCurrentWeekGMT() : Date {
    let today = new Date();
    let day = today.getDay();
    let diff = today.getDate() - day;
    today.setDate(diff);
    today = this.resetHours(today);
    today = this.fromLocalToGMT(today);
    return today;
  }

  getCurrentWeekLocal() : Date {
    let today = new Date();
    let day = today.getDay();
    let diff = today.getDate() - day;
    today.setDate(diff);
    today = this.resetHours(today);
    return today;
  }

  getTodayLocal() : Date {
    let today = new Date();
    today = this.resetHours(today);
    return today;
  }

  toString(date : Date) 
  {
      var mm = date.getMonth() + 1; // getMonth() is zero-based
      var dd = date.getDate();
      return [(mm>9 ? '' : '0') + mm, (dd>9 ? '' : '0') + dd,date.getFullYear()].join('\\');
  }

  toIsoString(date : Date) 
  {
      var mm = date.getMonth() + 1; // getMonth() is zero-based
      var dd = date.getDate();
      return [date.getFullYear(), (mm>9 ? '' : '0') + mm, (dd>9 ? '' : '0') + dd].join('-');
  }


  addDays(date: Date, days : number) 
  {
    var newDate = new Date(date);
    newDate.setDate(date.getDate() + days);
    return newDate;
}
}