import { combineReducers } from 'redux';

import { AuthTokenReducer } from '@reducers/auth-token.reducer';

export const Reducers = combineReducers({
    AuthTokenReducer
});