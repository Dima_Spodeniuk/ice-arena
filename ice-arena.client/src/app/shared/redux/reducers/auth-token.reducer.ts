const initialState:string = null;

export const AuthTokenMethods = {
    save: 'SAVE_AUTH_TOKEN',
    remove: 'REMOVE_AUTH_TOKEN'
}

export const AuthTokenReducer = (state = initialState, action:{type:string, payload:string}) => {
    switch(action.type) {
        case AuthTokenMethods.save:
            localStorage.setItem('kjfasdif9asfpo2fkjslidugbvcoSy02387hfinzlxcknjiuah3w80f7vbdxkcvc', action.payload);
            return state = action.payload;
        case AuthTokenMethods.remove:
            localStorage.removeItem('kjfasdif9asfpo2fkjslidugbvcoSy02387hfinzlxcknjiuah3w80f7vbdxkcvc');
            return state = null;
        default:
            return state;
    }
}