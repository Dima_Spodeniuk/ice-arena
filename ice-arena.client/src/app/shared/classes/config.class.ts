export class Config implements IConfig {
    id:string;
    dataLink:string;

	constructor(data?: IConfig) {
		if (data) {
			for (var property in data)
				if (data.hasOwnProperty(property)) {
					(<any>this)[property] = (<any>data)[property];
				}
		}
	}

	init(data?: any) {
		if (data) {
            this.id = data.id;
            this.dataLink = data.dataLink;
		}
	}

	static fromJS(data: any):Config {
		let result = new Config;
		result.init(data);
		return result;
	}

	toJSON(data?: any) {
		data = typeof data === 'object' ? data : {}
        data["Id"] = this.id;
        data["DataLink"] = this.dataLink;
		return data;
	}
}

export interface IConfig  {
    id:string;
    dataLink:string;
}