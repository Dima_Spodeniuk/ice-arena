export enum NotificationTypeEnum {
    error = 0,
    warning = 1,
    info = 2,
    success = 3
}