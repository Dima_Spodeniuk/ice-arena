import { Pipe, PipeTransform } from '@angular/core';

import { DateConvertService } from '@helpers/date-convert.service';

@Pipe({
    name: 'dateParser'
})

export class DateParserPipe implements PipeTransform {

    constructor(
        private dateConvertService:DateConvertService
    ) {}

    transform(value:string):Date {
        let year = value.slice(0, 4);
        let month = value.slice(4, 6);
        let day = value.slice(6, 8);
        let hours = value.slice(9, 11);
        let minutes = value.slice(11, 13);
        let seconds = value.slice(13, 15);
        let date = new Date(parseInt(year), (parseInt(month) - 1), parseInt(day), parseInt(hours), parseInt(minutes), parseInt(seconds));
        return date;
    }
}