import { NgModule } from '@angular/core';

import { DateParserPipe } from '@pipes/date-parser.pipe';

@NgModule({
  imports: [ ],
  declarations: [ 
    DateParserPipe
  ],
  exports: [
    DateParserPipe
  ]
})

export class SharedPipesModule { }