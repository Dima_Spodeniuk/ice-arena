import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

import { Config } from '@classes/config.class';

@Injectable({ providedIn: 'root' })
export class ConfigService {
	private apiUrl:string = '/api/config';
    constructor(private http: HttpClient) {}

	get():Observable<Config> {
		return this.http.get(this.apiUrl).pipe(map(res => res ? Config.fromJS(res) : null));
	}

    post(data:Config):Observable<any> {
		let headers = new HttpHeaders().set('Content-Type', 'application/json');
		return this.http.post<any>(this.apiUrl, data, { headers });
	}
}