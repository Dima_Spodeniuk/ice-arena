import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from "rxjs/operators";

@Injectable({ providedIn: 'root' })

export class CalendarService {
	private apiUrl:string = '/api/calendar';
    constructor(private http:HttpClient) {}

	get():Observable<string> {
		return this.http.get(this.apiUrl, {responseType: 'text'}).pipe(map(res => res ? res : null));
	}
}
