import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin/admin.component';
import { DefaultComponent } from './default/default.component';

export const router: Routes = [
    { path: '', component: DefaultComponent },
    { path: 'admin', component: AdminComponent },
    { path: '**', redirectTo: '' }
]

export const AppRoutes: ModuleWithProviders = RouterModule.forRoot(router);