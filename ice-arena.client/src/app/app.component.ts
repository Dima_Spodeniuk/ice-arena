import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

import { NotificationTypeEnum } from '@enums/notification.enum';

import { NotificationSubject } from '@subjects/notification.subject';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  notificationOptions = {
    showProgressBar: true,
    timeOut: 3400,
    lastOnBottom: true
  }

  constructor(
    private notificationSubject:NotificationSubject,
    private notificationsService:NotificationsService
  ) { }
  
  ngOnInit() {
    this.watchNotification();
  }

  watchNotification() {
    this.notificationSubject.watch().subscribe(data => this.notificationsService.create(data.title, data.text, this.getNotificationType(data.type)));
  }

  getNotificationType(type:number):string {
    switch (type) {
      case NotificationTypeEnum.success:
        return 'success';
      case NotificationTypeEnum.error:
        return 'error';
      case NotificationTypeEnum.info:
        return 'info';
      case NotificationTypeEnum.warning:
        return 'warn';
    }
  }
}