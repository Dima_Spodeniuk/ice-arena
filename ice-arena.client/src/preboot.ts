import { APP_INITIALIZER } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

import { Store } from '@store';

import { AuthTokenMethods } from '@reducers/auth-token.reducer';

const tokenHelper = new JwtHelperService();

const preBoot = ():() => Promise<any> => {
    return ():Promise<any> => {
        return new Promise((resolve, reject) => {
            let token = localStorage.getItem('kjfasdif9asfpo2fkjslidugbvcoSy02387hfinzlxcknjiuah3w80f7vbdxkcvc');

            if(token) {
                if(!tokenHelper.isTokenExpired(token)) {
                    Store.dispatch({ type: AuthTokenMethods.save, payload: token });
                } else {
                    let isAuth = Store.getState().AuthTokenReducer;

                    if(isAuth) Store.dispatch({ type: AuthTokenMethods.remove });
                }
            }
            resolve();
        });
    };
}

export const APP_PREBOOT = {
    provide: APP_INITIALIZER,
    useFactory: preBoot,
    deps: [],
    multi: true
}