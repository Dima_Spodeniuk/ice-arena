using System.Collections.Generic;

namespace ice_arena.server.Models {
    public class Response {
        public List<string> Errors { get; set; }
        public bool IsSuccess { get; set; }
    }
}