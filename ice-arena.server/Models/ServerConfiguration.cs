using System.ComponentModel.DataAnnotations.Schema;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace ice_arena.server.Models {
    public class ServerConfiguration {
        public string MongoConnectionString;
        public string MongoDatabase;
    }
}