using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ice_arena.server.Models {
    public class Login {

        [DisplayName("Email")]
        [Required(ErrorMessage = "is required")]
        public string Email { get; set; }

        [DisplayName("Password")]
        [Required(ErrorMessage = "is required")]
        public string Password { get; set; }
    }
}