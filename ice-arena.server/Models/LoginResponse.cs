namespace ice_arena.server.Models {
    public class LoginResponse : Response {
        public string Result { get; set; }
    }
}