using ice_arena.server.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

public class SeedDB {
    private IMongoDatabase _database = null;
    public SeedDB(IOptions<ServerConfiguration> settings) {
        var mongoClient = new MongoClient(settings.Value.MongoConnectionString);  
        if (mongoClient != null) _database = mongoClient.GetDatabase(settings.Value.MongoDatabase); 
    }

    public void SeedAdminUser() {

        var user = new User {
            FirstName = "Dima",
            LastName = "Spodeniuk",
            Email = "dima.spodeniuk@gmail.com",
            Password = "12345",
            Role = "Admin"
        };

        var collection = _database.GetCollection<User>("User");
        var items = collection.Find(_ => true).ToList();

        if(items.Count < 1) collection.InsertOne(user);
    }
}