using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using ice_arena.server.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ice_arena.server.Controllers {
    [Route("api/[controller]")]
    [ApiController]
    public class CalendarController : ControllerBase {
        private readonly IMongoDatabase _database;

        public CalendarController(IOptions<ServerConfiguration> settings) {  
            var mongoClient = new MongoClient(settings.Value.MongoConnectionString);  
            _database = mongoClient.GetDatabase(settings.Value.MongoDatabase);
        }

        [HttpGet]
        public string Get() {
            var configs = _database.GetCollection<Config>("Config").Find(_ => true).FirstOrDefault();
            if(configs != null && Uri.IsWellFormedUriString(configs.DataLink, UriKind.Absolute)) {
                try {
                    var data = new WebClient().DownloadString(configs.DataLink);
                    if(data is string) return data.Replace("\r", string.Empty);
                } catch (WebException ex) {

                }
            }
            return "";
        }
    }
}