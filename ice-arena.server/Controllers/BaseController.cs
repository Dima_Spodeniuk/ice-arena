using ice_arena.server.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ice_arena.server.Controllers {
    [Route("api/[controller]")]
    public abstract class BaseController : Controller {
        private readonly IMongoDatabase _database = null;
        public BaseController() { }

        protected string GenerateAuthToken() {
            return "cool token";
        }

        public IMongoDatabase GetMongoDB(IOptions<ServerConfiguration> settings) {
            var mongoClient = new MongoClient(settings.Value.MongoConnectionString);  
            return mongoClient.GetDatabase(settings.Value.MongoDatabase);  
        }
    }
}
