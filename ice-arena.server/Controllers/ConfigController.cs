using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using ice_arena.server.Helpers;
using ice_arena.server.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace ice_arena.server.Controllers {
    [Route("api/[controller]")]
    public class ConfigController : BaseController {
        private readonly IMongoDatabase _database;

        public ConfigController(IOptions<ServerConfiguration> settings) {  
            var mongoClient = new MongoClient(settings.Value.MongoConnectionString);  
            _database = mongoClient.GetDatabase(settings.Value.MongoDatabase);
        } 

        [HttpGet]
        public Config Get() {
            return _database.GetCollection<Config>("Config").Find(_ => true).FirstOrDefault();
        }

        [HttpPost]
        public bool Post([FromBody]Config value) {
            var collection = _database.GetCollection<Config>("Config");
            var items = collection.Find(_ => true).ToList();

            if(items.Count < 1) {
                collection.InsertOne(value);
            } else {
                collection.ReplaceOneAsync(c => c.Id == value.Id, value);
            }

            return true;
        }
    }
}
