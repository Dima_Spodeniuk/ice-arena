using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ice_arena.server.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;

namespace ice_arena.server.Controllers {
    [Route("api/[controller]")]
    public class LoginController : BaseController {
        private readonly IMongoDatabase _database;
        private readonly IConfiguration _config;

        public LoginController(IOptions<ServerConfiguration> settings, IConfiguration config) {  
            var mongoClient = new MongoClient(settings.Value.MongoConnectionString);  
            _database = mongoClient.GetDatabase(settings.Value.MongoDatabase);

            _config = config;
        } 

        [HttpPost]
        public LoginResponse Post([FromBody]Login value) {
            var errors = new List<string>();
            var response = new LoginResponse();

            if(ModelState.IsValid) {
                var user = _database.GetCollection<User>("User").Find(_ => _.Email == value.Email && _.Password == value.Password).FirstOrDefault();
                
                if(user != null && user.Password == value.Password) {
                    response.Result = GenerateToken(user);
                } else {
                    errors.Add("Wrong email or password");
                }
            } else {
                foreach(var item in ModelState) {
                    var key = item.Key;
                    foreach(var error in item.Value.Errors) {
                        errors.Add(key + ' ' + error.ErrorMessage);
                    }
                }
            }

            if(errors.Count > 0) {
                response.Errors = errors;
                response.IsSuccess = false;
                response.Result = null;
            } else {
                response.Errors = null;
                response.IsSuccess = true;
            }

            return response;
        }

        private string GenerateToken(User user) {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            //var userInfo = Newtonsoft.Json.JsonConvert.SerializeObject(user);

            var claimsData = new[] { 
                //new Claim("userInfo", userInfo),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role)
            };

            var token = new JwtSecurityToken(
                            _config["Jwt:Issuer"],
                            _config["Jwt:Issuer"],
                            claims: claimsData,
                            expires: DateTime.Now.AddYears(1),
                            signingCredentials: creds
                        );

            return  new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
